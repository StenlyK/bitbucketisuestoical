<?php

/**
 * Homepage presenter.
 */
class BitbucketPresenter extends BasePresenter
{

	public function renderDefault($user)
	{
		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: inline; filename='.$user.'.ics');
		$url = $this->context->params['api'].'repositories/'.$this->context->params['repo_slug'].'/';
		$result = array();
		foreach($this->context->params['repos'] as $repo)
		{
			$isueUrl = $url.$repo.'/issues?responsible='.$user.'&status=open&status=new';
			$isueJson = file_get_contents($isueUrl);
			$isues = json_decode($isueJson);
			$isues = array_shift($isues->issues);
			if (!is_null($isues))
			{
				$isues->repo = $repo;
				$result[] = $isues;
			}

		}
		$this->template->user = $user;
		$this->template->data = $result;
	}

}
